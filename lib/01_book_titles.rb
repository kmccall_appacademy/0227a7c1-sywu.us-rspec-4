class Book
  # TODO: your code goes here!
  attr_accessor :title
  
  def title
    pool = ["the", "and", "a", "an", "of", "in"]
    title = []
    @title.split.each_with_index do |word,index|
      if pool.include?(word) && index > 0
        title << word
      else
        title << word.capitalize
      end
    end
    title.join(" ")
  end
end
