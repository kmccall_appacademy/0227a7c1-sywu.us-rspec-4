class Dictionary
  # TODO: your code goes here!
  attr_accessor :entries
  def initialize
    @entries = {}
  end
  
  def add(hash)
    if hash.is_a?(Hash)
      @entries[hash.keys[0]] = hash.values[0]
    else
      @entries[hash] = nil
    end
  end
  
  def keywords
    @entries.keys.sort
  end
  
  def include?(input)
    @entries.keys.each do |key|
      return true if key == input
    end
    return false
  end
  
  def find(input)
    @entries.select do |key, value|
      key.include?(input)
    end
  end
  
  def printable
    string = []
    @entries.sort.each do |key,val|
      string << "[#{key}] \"#{val}\""
    end
    string.join("\n")
  end
  
end
