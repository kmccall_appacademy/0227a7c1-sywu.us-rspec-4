class Temperature
  # TODO: your code goes here!
  attr_accessor :symbol , :temp
  def initialize(hash)
    @symbol = hash.keys[0]
    @temp = hash.values[0]
  end
  
  def in_fahrenheit
    if @symbol == :f
      @temp
    else
      (@temp * 9.0 / 5) + 32
    end
  end
  
  def in_celsius
    if @symbol == :c
      @temp
    else
      (@temp - 32) * 5 / 9.0
    end
  end
  
  class << self
    def from_celsius(temp)
      new(:c=>temp)
    end
    
    
    def from_fahrenheit(temp)
      new(:f=>temp)
    end
  end
  
end

class Celsius < Temperature
  def initialize(temp)
    @symbol = :c
    @temp = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @symbol = :f
    @temp = temp
  end
end
